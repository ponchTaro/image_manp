#include <stdio.h>
#include "defs.h"
#include "image.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	if (argc < 2) {
		fprintf(stderr, "実行方法: ./ex3 [入力画像]\n");
		return 1;
	}

	// 入力画像を読み込む
	Image I;
	if (I.Load(argv[1]) != SOUSEI_OK) {
		fprintf(stderr, "エラー： 入力画像の読み込みに失敗しました．\n");
		return 1;
	}

	// 画像の幅と高さを取得
	int W = I.Width();
	int H = I.Height();

	// 出力画像を用意する
	Image C(W, H);

	// 各ピクセルに対し・・・
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {
			// 【 TODO 】元の色の補色をセット
			C(x, y, R) = 255 - I(x, y, R);
			C(x, y, G) = 255 - I(x, y, G);
			C(x, y, B) = 255 - I(x, y, B);
		}
	}

	// 画像を表示
	C.Show();

	return 0;
}
