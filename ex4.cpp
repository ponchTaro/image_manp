#include <stdio.h>
#include "defs.h"
#include "image.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	if (argc < 2) {
		fprintf(stderr, "実行方法: ./ex4 [入力画像]\n");
		return 1;
	}

	// 入力画像を読み込む
	Image I;
	if (I.Load(argv[1]) != SOUSEI_OK) {
		fprintf(stderr, "エラー： 入力画像の読み込みに失敗しました．\n");
		return 1;
	}

	// 画像の幅と高さを取得
	int W = I.Width();
	int H = I.Height();

	// 出力画像を用意
	Image E(W, H);

	// 各ピクセルに対し・・・
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {
			// 【 TODO 】指定の式に従って色をセット
			if (x == 0 && y == 0) {
				E(x, y, R) = I(0, 0, R);
				E(x, y, G) = I(0, 0, G);
				E(x, y, B) = I(0, 0, B);
			}else if (1 <= x && x <= W && y == 0) {
				E(x, y, R) = I(x, 0, R) - I(x-1, 0, R);
				E(x, y, G) = I(x, 0, G) - I(x-1, 0, G);
				E(x, y, B) = I(x, 0, B) - I(x-1, 0, B);
			}else if (1 <= y && y <= H) {
				E(x, y, R) = I(x, y, R) - I(x, y-1, R);			
				E(x, y, G) = I(x, y, G) - I(x, y-1, G);			
				E(x, y, B) = I(x, y, B) - I(x, y-1, B);			
			}
		}
	}

	// 画像を表示
	E.Show();

	return 0;
}
