# マクロ
PRG_A := ex1 ex2 ex3 ex4 ex8
PRG_B := ex5 ex6 ex7-1 ex7-2 ex10-1 ex10-2
PRG_C := ex11-1 ex11-2 ex12-1 ex12-2 PSNR comp decomp k-means-color-palet

PROGRAMS := $(PRG_A) $(PRG_B) $(PRG_C)
HEADERS_A := defs.h image.h
HEADERS_B := bitseq.h
HEADERS_C := tools.h

# OpenCV用オプション
CV_CFLAGS := `pkg-config --cflags opencv`
CV_LIBS := `pkg-config --libs opencv`

# C++コンパイラ
CXX = g++

# コンパイルオプション
CXXFLAGS = -std=c++11 -Wall -O2


### 以下、レシピ ###

.PHONY: all clean

all: $(PROGRAMS)

clean:
	rm -rf $(PROGRAMS)

$(PROGRAMS): %: %.cpp
	$(CXX) -o $@ $< $(CV_LIBS)

$(PRG_A): %: $(HEADERS_A)

$(PRG_B): %: $(HEADERS_A) $(HEADERS_B)

$(PRG_C): %: $(HEADERS_A) $(HEADERS_B) $(HEADERS_C)
