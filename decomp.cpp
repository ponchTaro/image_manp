#include <stdio.h>
#include "defs.h"
#include "image.h"
#include "tools.h"
#include "bitseq.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	if (argc < 3) {
		fprintf(stderr, "実行方法: ./decomp [圧縮ファイル] [復元画像]\n");
		return 1;
	}

	// 入力ファイルからビット列を読み込む
	BitSeq seq;
	seq.Load(argv[1]);

	// ビット列の 0 ビット目から 32 ビット分を抜き出す ⇒ 画像の幅を表す整数値に変換
	BitSeq Ws = seq.SubSeq(0, 32);
	int W = ToInt32(Ws);

	// ビット列の 32 ビット目から 32 ビット分を抜き出す ⇒ 画像の高さを表す整数値に変換
	BitSeq Hs = seq.SubSeq(32, 32);
	int H = ToInt32(Hs);

	// 幅 W, 高さ H の復元画像を用意
	Image I(W, H);




	/*  このあたりに画像復元処理を記述する  */




	// 画像を表示
	I.Show();

	// 画像を保存
	I.Save(argv[2]);

	return 0;
}

