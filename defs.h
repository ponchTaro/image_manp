#ifndef DEFS_H__
#define DEFS_H__


// 創成実験用の名前空間
namespace Sousei
{
	// 関数等のエラーコード
	enum SOUSEI_ERROR_CODE {
		SOUSEI_OK, // 正常終了
		SOUSEI_FAILURE // それ以外
	};

	// コピー禁止クラスを定義するための基底クラス
	class Noncopyable
	{
	protected:
		Noncopyable() {}
		~Noncopyable() {}

	private:
		void operator =(const Noncopyable& src);
		Noncopyable(const Noncopyable& src);
	};
}


#endif
