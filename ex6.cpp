#include <stdio.h>
#include "defs.h"
#include "image.h"
#include "bitseq.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	if (argc < 2) {
		fprintf(stderr, "実行方法: ./ex6 [入力データファイル]\n");
		return 1;
	}

	// 入力データファイルからビット列を読み込む
	BitSeq seq;
	seq.Load(argv[1]);

	// ビット列の 0 ビット目から 32 ビット分を抜き出す ⇒ 画像の幅を表す整数値に変換
	BitSeq Ws = seq.SubSeq(0, 32);
	int W = ToInt32(Ws);

	// ビット列の 32 ビット目から 32 ビット分を抜き出す ⇒ 画像の高さを表す整数値に変換
	BitSeq Hs = seq.SubSeq(32, 32);
	int H = ToInt32(Hs);

	// 幅 W, 高さ H の画像を用意
	Image I(W, H);

	// ビット列の 64 ビット目以降には 8 ビット区切りで画素値が格納されているので・・・
	int m = 64;
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {
			BitSeq Rs = seq.SubSeq(m, 8); // 8 ビットずつ読み出す
			m += 8;
			BitSeq Gs = seq.SubSeq(m, 8);
			m += 8;
			BitSeq Bs = seq.SubSeq(m, 8);
			m += 8;
			I(x, y, R) = ToInt8(Rs); // 色を表す整数値に変換し，各ピクセルにセット
			I(x, y, G) = ToInt8(Gs);
			I(x, y, B) = ToInt8(Bs);
		}
	}

	// 画像を表示
	I.Show();

	return 0;
}
