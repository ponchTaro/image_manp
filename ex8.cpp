#include <stdio.h>
#include "defs.h"
#include "image.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	if (argc < 3) {
		fprintf(stderr, "実行方法: ./ex8 [画像1] [画像2]\n");
		return 1;
	}

	// 画像1を読み込む
	Image I1;
	I1.Load(argv[1]);

	// 画像2を読み込む
	Image I2;
	I2.Load(argv[2]);

	// 二枚の画像のサイズが一致しているかチェック
	if (I1.Width() != I2.Width() || I1.Height() != I2.Height()) {
		fprintf(stderr, "エラー： 画像1 と 画像2 のサイズが一致しません．\n");
		return 1;
	}

	// 画像の幅と高さを取得
	int W = I1.Width();
	int H = I1.Height();

	// 出力画像を用意
	Image D(W, H);

	// 各ピクセルに対し・・・
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {
			// 【 TODO 】指定の式に従って色をセット
			D(x, y, R) = I1(x, y, R) - I2(x, y, R);
			D(x, y, G) = I1(x, y, G) - I2(x, y, G);
			D(x, y, B) = I1(x, y, B) - I1(x, y, B);
		}
	}

	// 画像を表示
	D.Show();

	return 0;
}
