#include <stdio.h>
#include "defs.h"
#include "image.h"
#include "tools.h"
#include "bitseq.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	if (argc < 3) {
		fprintf(stderr, "実行方法: ./PSNR [画像1] [画像2]\n");
		return 1;
	}

	// 画像1を読み込む
	Image I1;
	I1.Load(argv[1]);

	// 画像2を読み込む
	Image I2;
	I2.Load(argv[2]);

	// 二枚の画像のサイズが一致しているかチェック
	if (I1.Width() != I2.Width() || I1.Height() != I2.Height()) {
		fprintf(stderr, "エラー： 画像1 と 画像2 のサイズが一致しません．\n");
		return 1;
	}

	// 画像の幅と高さを取得
	int W = I1.Width();
	int H = I1.Height();

	// MSEを計算
	double mse = 0;
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {
			mse += square((double)I1(x, y, R) - I2(x, y, R));
			mse += square((double)I1(x, y, G) - I2(x, y, G));
			mse += square((double)I1(x, y, B) - I2(x, y, B));
		}
	}
	if (mse == 0) {
		fprintf(stdout, "＋無限大 (positive infinity)\n");
		return 0;
	}

	// PSNRを計算
	double psnr = 20 * log10(255 / sqrt(mse / (3 * W * H)));
	fprintf(stdout, "%.2f\n", psnr);

	return 0;
}
