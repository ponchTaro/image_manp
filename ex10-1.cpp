#include <stdio.h>
#include "defs.h"
#include "image.h"
#include "bitseq.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	const int n = 4; // [パラメータ]圧縮画像の階調数は 2^n

	if (argc < 3) {
		fprintf(stderr, "実行方法: ./ex10-1 [入力画像] [圧縮ファイル]\n");
		return 1;
	}

	// 入力画像を読み込む
	Image I;
	if (I.Load(argv[1]) != SOUSEI_OK) {
		fprintf(stderr, "エラー： 入力画像の読み込みに失敗しました．\n");
		return 1;
	}

	// 入力画像の幅と高さを取得
	int W = I.Width();
	int H = I.Height();

	// 出力用ビット列
	BitSeq seq;

	// 画像の幅と高さ（復元時に必要）の値を32ビットのビット列に変換し，出力用ビット列に連結
	BitSeq Ws = To32BitCode(W);
	BitSeq Hs = To32BitCode(H);
	seq += Ws;
	seq += Hs;

	// 各ピクセルに対し・・・
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {

			// R, G, B の値をそれぞれ8ビットのビット列に変換
			BitSeq Rs = To8BitCode(I(x, y, R));
			BitSeq Gs = To8BitCode(I(x, y, G));
			BitSeq Bs = To8BitCode(I(x, y, B));

			// 上位 n ビットのみを残し，下位 8-n ビットを切り捨てる
			Rs = Rs.SubSeq(0, n); // 0 ビット目から n ビット分を残す
			Gs = Gs.SubSeq(0, n);
			Bs = Bs.SubSeq(0, n);

			// 残した部分を出力用ビット列に連結
			seq += Rs;
			seq += Gs;
			seq += Bs;
		}
	}

	// 出力用ビット列をファイルに保存
	seq.Save(argv[2]);

	return 0;
}
