#ifndef BITSEQ_H__
#define BITSEQ_H__


#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
#include "defs.h"


namespace Sousei
{
	// ビット列（0 または 1 の系列）を表現するクラス
	class BitSeq
	{
	private:
		char dmy;
		std::string seq;

		// 所与のビット列が不正か否かをチェック
		static bool check(const std::string& code)
		{
			if (code.empty()) return false;
			for (int i = 0; i < static_cast<int>(code.size()); ++i) {
				if (code[i] != '0' && code[i] != '1') return false;
			}
			return true;
		}

	public:
		// コンストラクタ
		BitSeq() : dmy(0), seq("") {}

		// コピーコンストラクタ（文字列型変数による初期化）
		BitSeq(const std::string& str) : dmy(0), seq("")
		{
			if (check(str)) seq = str;
			else {
				fprintf(stderr, "警告： 不正なビット列が指定されました．\n");
			}
		}

		// コピーコンストラクタ（文字配列による初期化）
		BitSeq(const char* p) : dmy(0), seq("")
		{
			std::string str;
			if (p != NULL && check(str = std::string(p))) seq = str;
			else {
				fprintf(stderr, "警告： 不正なビット列が指定されました．\n");
			}
		}

		// 文字列型変数の代入
		BitSeq& operator=(const std::string& str)
		{
			if (check(str)) seq = str;
			else {
				fprintf(stderr, "警告： 不正なビット列が指定されました．\n");
			}
			return *this;
		}

		// 文字配列の代入
		BitSeq& operator=(const char* p)
		{
			std::string str;
			if (p != NULL && check(str = std::string(p))) seq = str;
			else {
				fprintf(stderr, "警告： 不正なビット列が指定されました．\n");
			}
			return *this;
		}

		// 値の取得／設定
		const char operator[](int n) const
		{
			if (0 <= n && n < static_cast<int>(seq.size())) return seq[n];
			else return 0;
		}
		char& operator[](int n)
		{
			if (0 <= n && n < static_cast<int>(seq.size())) return seq[n];
			else return dmy;
		}

		// 長さの取得
		int Length() const
		{
			return static_cast<int>(seq.size());
		}

		// 等価演算子
		bool operator==(const BitSeq& obj) const
		{
			if (seq == obj.seq) return true;
			else return false;
		}
		bool operator==(const std::string& str) const
		{
			if (seq == str) return true;
			else return false;
		}
		bool operator==(const char* p) const
		{
			if (p == NULL) return false;
			if (seq == std::string(p)) return true;
			else return false;
		}

		// ビット列の連結
		BitSeq operator+(const BitSeq& obj) const
		{
			BitSeq s;
			s.seq = seq + obj.seq;
			return s;
		}
		BitSeq operator+(const std::string& str) const
		{
			BitSeq s;
			if (check(str)) s.seq = seq + str;
			else {
				fprintf(stderr, "警告： 不正なビット列が指定されました．\n");
				s.seq = seq;
			}
			return s;
		}
		BitSeq operator+(const char* p) const
		{
			BitSeq s;
			std::string str;
			if (p != NULL && check(str = std::string(p))) s.seq = seq + str;
			else {
				fprintf(stderr, "警告： 不正なビット列が指定されました．\n");
				s.seq = seq;
			}
			return s;
		}
		BitSeq& operator+=(const BitSeq& obj)
		{
			if (this == &obj) {
				std::string temp = seq;
				seq += temp;
			}
			else {
				seq += obj.seq;
			}
			return *this;
		}
		BitSeq& operator+=(const std::string& str)
		{
			if (check(str)) seq += str;
			else {
				fprintf(stderr, "警告： 不正なビット列が指定されました．\n");
			}
			return *this;
		}
		BitSeq& operator+=(const char* p)
		{
			std::string str;
			if (p != NULL && check(str = std::string(p))) seq += str;
			else {
				fprintf(stderr, "警告： 不正なビット列が指定されました．\n");
			}
			return *this;
		}

		// 現在の内容をクリア
		SOUSEI_ERROR_CODE Clear()
		{
			seq.clear();
			dmy = 0;
		}

		// 現在の内容を出力
		SOUSEI_ERROR_CODE Print() const
		{
			if (seq.empty()) fprintf(stdout, "(empty)\n");
			else fprintf(stdout, "%s\n", seq.c_str());
			return SOUSEI_OK;
		}

		// 部分列の取得
		BitSeq SubSeq(int pos, int length) const
		{
			BitSeq s;
			if (0 <= pos && pos < static_cast<int>(seq.size()) && 0 < length) {
				s.seq = seq.substr(pos, length);
			}
			else {
				fprintf(stderr, "警告： 部分列の指定値に誤りがあります．\n");
			}
			return s;
		}

		// 現在の内容をファイルに保存
		SOUSEI_ERROR_CODE Save(const std::string& filename) const
		{
			FILE* fp = NULL;
			if (seq.empty()) return SOUSEI_FAILURE;
			if ((fp = fopen(filename.c_str(), "wb")) == NULL) {
				fprintf(stderr, "警告： 出力ファイルのオープンに失敗しました．\n");
				return SOUSEI_FAILURE;
			}
			else {
				int length = static_cast<int>(seq.size());
				fwrite(&length, sizeof(int), 1, fp);
				for (int i = 0; i < length; i += 8) {
					uchar c = 0;
					uchar d = 0x01;
					std::string code = seq.substr(i, 8);
					if (code.size() < 8) {
						const int n = 8 - static_cast<int>(code.size());
						for (int j = 0; j < n; ++j) code += "0";
					}
					for (int j = 0; j < 8; ++j, d <<= 1) {
						if (code[j] == '1') c |= d;
					}
					fwrite(&c, sizeof(uchar), 1, fp);
				}
				fclose(fp);
				return SOUSEI_OK;
			}
		}

		// ファイルからビット列を読み込む
		SOUSEI_ERROR_CODE Load(const std::string& filename)
		{
			FILE* fp = NULL;
			if ((fp = fopen(filename.c_str(), "rb")) == NULL) {
				fprintf(stderr, "警告： 入力ファイルのオープンに失敗しました．\n");
				return SOUSEI_FAILURE;
			}
			else {
				int length = 0;
				uchar c = 0;
				if (fread(&length, sizeof(int), 1, fp) != 1) {
					fprintf(stderr, "警告： 不正な入力ファイルです．\n");
					return SOUSEI_FAILURE;
				}
				seq.clear();
				while (fread(&c, sizeof(uchar), 1, fp) == 1) {
					uchar d = 0x01;
					std::string code = "00000000";
					for (int j = 0; j < 8; ++j, d <<= 1) {
						if ((c & d) != 0) code[j] = '1';
					}
					seq += code;
				}
				if (static_cast<int>(seq.size()) > length) seq = seq.substr(0, length);
				fclose(fp);
				return SOUSEI_OK;
			}
		}
	};

	// 整数値を32ビットのビット列に変換する
	BitSeq To32BitCode(int n)
	{
		BitSeq s;
		int d = 0x40000000;
		if (n < 0) s += "1"; else s += "0"; // 最上位ビット
		for (int i = 0; i < 31; ++i, d >>= 1) {
			if ((n & d) != 0) s += "1"; else s += "0"; // 2ビット目以降
		}
		return s;
	}

	// 整数値を 0 ～ 255 の範囲に丸めたのち，8ビットのビット列に変換する
	BitSeq To8BitCode(int n)
	{
		BitSeq s;
		uchar d = 0x80;
		uchar c = cv::saturate_cast<uchar>(n);
		for (int i = 0; i < 8; ++i, d >>= 1) {
			if ((c & d) != 0) s += "1"; else s += "0";
		}
		return s;
	}

	// ビット列の先頭 32 ビットを読み込んで整数値に変換
	int ToInt32(const BitSeq& s)
	{
		int n = 0;
		unsigned int m = 0;
		unsigned int d = 0x80000000;
		for (int i = 0; i < 32; ++i, d >>= 1) {
			if (s[i] == '1') m |= d;
		}
		memcpy(&n, &m, sizeof(int));
		return n;
	}

	// ビット列の先頭 8 ビットを読み込んで整数値に変換
	int ToInt8(const BitSeq& s)
	{
		int n = 0;
		uchar m = 0;
		uchar d = 0x80;
		for (int i = 0; i < 8; ++i, d >>= 1) {
			if (s[i] == '1') m |= d;
		}
		n = cv::saturate_cast<int>(m);
		return n;
	}
}


#endif
