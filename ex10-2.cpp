#include <stdio.h>
#include "defs.h"
#include "image.h"
#include "bitseq.h"


using namespace Sousei;


// メイン関数： ここから処理を開始
int main(int argc, char** argv)
{
	const int n = 4; // [パラメータ]圧縮画像の階調数は 2^n

	if (argc < 3) {
		fprintf(stderr, "実行方法: ./ex10-2 [圧縮ファイル] [復元画像]\n");
		return 1;
	}

	// 入力ファイルからビット列を読み込む
	BitSeq seq;
	seq.Load(argv[1]);

	// ビット列の 0 ビット目から 32 ビット分を抜き出す ⇒ 画像の幅を表す整数値に変換
	BitSeq Ws = seq.SubSeq(0, 32);
	int W = ToInt32(Ws);

	// ビット列の 32 ビット目から 32 ビット分を抜き出す ⇒ 画像の高さを表す整数値に変換
	BitSeq Hs = seq.SubSeq(32, 32);
	int H = ToInt32(Hs);

	// 幅 W, 高さ H の復元画像を用意
	Image I(W, H);

	// ビット列の 64 ビット目以降から画素値を読み出し，低解像度画像にセット
	int m = 64;
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {

			// n ビットずつ読み出す
			BitSeq Rs = seq.SubSeq(m, n);
			m += n;
			BitSeq Gs = seq.SubSeq(m, n);
			m += n;
			BitSeq Bs = seq.SubSeq(m, n);
			m += n;

			// 下位 8-n ビットを 0 で埋める
			for (int j = 0; j < 8 - n; ++j) {
				Rs += "0";
				Gs += "0";
				Bs += "0";
			}

			// 色を表す整数値に変換し，各ピクセルにセット
			I(x, y, R) = ToInt8(Rs);
			I(x, y, G) = ToInt8(Gs);
			I(x, y, B) = ToInt8(Bs);
		}
	}

	// 画像を表示
	I.Show();

	// 画像を保存
	I.Save(argv[2]);

	return 0;
}
