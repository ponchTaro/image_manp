#ifndef TOOLS_H__
#define TOOLS_H__


#include <map>
#include <string>
#include <vector>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include "defs.h"
#include "bitseq.h"


/*

[vector について]

C++では配列を扱うときに vector を使う．
vector<int> は int 型の配列を，vector<double> は double 型の配列を表す．
つまり，

	vector<int> a;		・・・(1)

と

	int a[10];			・・・(2)

は大体同じ意味になる．

(2)の場合，配列 a のサイズは 10 で固定であるが，
(1)の場合，配列 a のサイズは後で変えられる（ちなみに宣言時点での a のサイズは 0 ）．
これが vector の便利なところである．

vector 配列のサイズは push_back() により変更されることが多い．
push_back() は末尾に一つ要素を追加する命令である．
いま，

	vector<int> a;

が宣言されているとする．
ここで

	a.push_back(3);

と書くと，a のサイズは一つ増えて 0 から 1 になり，a[0] == 3 となる．
この後さらに

	a.push_back(5);

と書くと，サイズはさらに一つ増えて 1 から 2 となり，a[0] == 3, a[1] == 5 となる．

一度作った要素には，通常の配列と同様，a[0] や a[1] などの書き方でアクセスできる．
上書きも可能で，

	a[1] = 10;

とすれば a[1] == 5 だったものが a[1] == 10 に変わる．

なお，現在のサイズを取得するためには size() を使う

	int n = a.size();

と書くと，整数変数 n に配列 a 現在のサイズが格納される．

*/


namespace Sousei
{
	using namespace std;

	// 二乗を求める関数
	template<typename tn> tn square(tn a) { return a * a; }

	// ランレングス表現を扱うための構造体
	typedef struct {
		int value;
		int count;
	} run_length_unit;

	// int型配列をランレングス表現に変換
	SOUSEI_ERROR_CODE ToRunLength(
		const vector<int>& a, // [入力] 整数の一次元配列
		vector<run_length_unit>& b, // [出力] ランレングス表現
		const int max_length = 255)
	{
		const int size = static_cast<int>(a.size());
		run_length_unit u;

		if (size <= 0 || max_length < 1) return SOUSEI_FAILURE;

		u.count = 1;
		u.value = a[0];
		for (int i = 1; i < size; ++i) {
			if (a[i] != u.value || u.count == max_length) {
				b.push_back(u);
				u.value = a[i];
				u.count = 1;
			}
			else u.count++;
		}
		b.push_back(u);

		return SOUSEI_OK;
	}

	// ランレングス表現をint型配列に変換
	SOUSEI_ERROR_CODE ToArray(
		const vector<run_length_unit>& a, // [入力] ランレングス表現
		vector<int>& b) // [出力] 整数の一次元配列
	{
		const int size = static_cast<int>(a.size());

		if (size <= 0) return SOUSEI_FAILURE;

		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < a[i].count; ++j) {
				b.push_back(a[i].value);
			}
		}

		return SOUSEI_OK;
	}

	// カラーパレットを取り扱うためのクラス
	class ColorPalette : private Noncopyable
	{
	private:
		vector<cv::Vec3b> colors;
		vector<BitSeq> codes;

	public:
		// 色とビット列の対応関係を一つ登録
		SOUSEI_ERROR_CODE Set(int r, int g, int b, const BitSeq& s)
		{
			uchar ur = cv::saturate_cast<uchar>(r);
			uchar ug = cv::saturate_cast<uchar>(g);
			uchar ub = cv::saturate_cast<uchar>(b);

			const int size = static_cast<int>(colors.size());
			for (int i = 0; i < size; ++i) {
				if (colors[i][0] == ur && colors[i][1] == ug && colors[i][2] == ub) {
					return SOUSEI_FAILURE;
				}
				if (codes[i] == s) {
					return SOUSEI_FAILURE;
				}
			}
			colors.push_back(cv::Vec3b(ur, ug, ub));
			codes.push_back(s);

			return SOUSEI_OK;
		}

		// 現在のカラーパレットをクリア
		SOUSEI_ERROR_CODE Clear()
		{
			colors.clear();
			codes.clear();
		}

		// 登録済みの色の数を返す
		int Size() const
		{
			return static_cast<int>(colors.size());
		}

		// 要素へのアクセス
		int GetColor(int n, const COLOR_CHANNEL c) const
		{
			const int size = static_cast<int>(colors.size());
			if (0 <= n && n < size) {
				switch (c) {
				case R: return cv::saturate_cast<int>(colors[n][0]);
				case G: return cv::saturate_cast<int>(colors[n][1]);
				case B: return cv::saturate_cast<int>(colors[n][2]);
				}
			}
			fprintf(stderr, "警告： 配列外アクセスが発生しました．\n");
			return -1;
		}
		BitSeq GetCode(int n) const
		{
			BitSeq s;
			const int size = static_cast<int>(codes.size());
			if (0 <= n && n < size) return codes[n];
			else {
				fprintf(stderr, "警告： 配列外アクセスが発生しました．\n");
				return s;
			}
		}

		// 指定のビット列に対応する色を取得
		int ToColor(const BitSeq& s, const COLOR_CHANNEL c) const
		{
			const int size = static_cast<int>(codes.size());
			for (int i = 0; i < size; ++i) {
				if (codes[i] == s) {
					switch (c) {
					case R: return cv::saturate_cast<int>(colors[i][0]);
					case G: return cv::saturate_cast<int>(colors[i][1]);
					case B: return cv::saturate_cast<int>(colors[i][2]);
					}
				}
			}
			fprintf(stderr, "警告： 指定されたビット列に対応する色は登録されていません．\n");
			return -1;
		}

		// 指定の色に最も近い色を探し，それに対応するビット列を返す
		BitSeq ToCode(int r, int g, int b) const
		{
			BitSeq s;
			const int size = static_cast<int>(colors.size());

			if (size <= 0) return s;

			int n = 0;
			int cr = cv::saturate_cast<int>(colors[0][0]);
			int cg = cv::saturate_cast<int>(colors[0][1]);
			int cb = cv::saturate_cast<int>(colors[0][2]);
			int d_min = square(cr - r) + square(cg - g) + square(cb - b);
			for (int i = 1; i < size; ++i) {
				cr = cv::saturate_cast<int>(colors[i][0]);
				cg = cv::saturate_cast<int>(colors[i][1]);
				cb = cv::saturate_cast<int>(colors[i][2]);
				int d = square(cr - r) + square(cg - g) + square(cb - b);
				if (d < d_min) {
					d_min = d;
					n = i;
				}
			}

			return codes[n];
		}
	};
}


#endif
